# video-timecode-rs

Rust library for manipulating SMPTE timecodes.

## Toolchain

This library requires Rust nightly toolchain until `step_trait` is stabilized
(https://github.com/rust-lang/rust/issues/42168).

## Credits

* Inspired by the timecode library for Python: https://github.com/eoyilmaz/timecode
